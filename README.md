# MCOI PLATFORM with XU5 MODULE

## REPOSITORY STRUCTURE
```bash
├── ddesign
│   ├── modules
│   ├── README.md
│   └── testing_Enclustra_PE1
│       ├── design_changelog.txt
│       └── kit_changelog.txt
├── linux
│   └── README.md
├── pcb_design
├── project_notes.md
└── REDME.md
```
### directories
 * ddesign - development and final digital design for the platform
 * linux - linux distribution running on the internal ARM MCU
 * pcb_design - altium designer files with board schematics and pcb layout

