# DIGITAL DESIGN
- exact name of module used in following design - Mercury XU5 ME-XU5-4EV-1I-D11E-G1-R1.2

## Ultrascale project -> ultrascale+ project
- changed project device
- chanded pins according to schematics (enclustra)
- changed mgt width of signals (need change array definitions in folder close to
ip cores definiton)
- changed ibufds\_gt3 -> ibufds\_gt4(its intended for ultra+)
- pins wrong -> OK fixed
- ignoring dedicated routing ???
- set_property CONFIG.INCLUDE_CPLL_CAL 0 [get_ips xlx_zup_mgt_ip] => to generate
  transceiver without pll calibration -> could possibly cause fails
- created design with ARM part 
- forwarded clock from IBUFDS_GTE4 -> to IOB pin header

## CHANGES in current design
- uncommen fabricClk... sysclk_300 
- comment cpu_reset, sysclk in constraints
- uncomment sysclk_300 and clk_125
- comment buffer sysclk_inst  

## TODO
- resolve timing error in gbt!!!
- regenerate new csv pinout from board
- createt project tcl script generator
- create pulling data from i2c temperature sensor

## OTHER NOTES
- design is working with external clocking source even if you use it as a 
  reference clock 
- be aware of different names of XU5 modules. In schematics you can find module 
  pins connected to the same fpga pin (like MGT_B224_REFCLK0_Y6_B66_L11_D4_P).
  Check the the [Pinout-Assembly-Variants](https://download.enclustra.com/public_files/SoC_Modules/Mercury_XU5/Mercury_XU5-R1_FPGA_Pinout_Assembly_Variants.xlsx) for
  more information. For this case I wrote an easy script which parses the csv file
  from enclustra and csv from altium designer and creates constraints file for digital design.

## PROJECT USAGE
Current project consists of more top modules for better testing of single parts
of the design. To run certain modul you have to enable in Vivado the exact
constraints which are named the same as the top module ending with
"constraints". So if you for example want to choose top module "qspi_load_test"
you have to disable in constraints folder in Project manager "Hierarchy" all
constraint files exept "qspi_load_test_constraints.xdc"
