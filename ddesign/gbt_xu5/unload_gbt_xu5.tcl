
set script_path [file dirname [info script]]
set gbt_location $script_path/../modules/Zynq_USplus_gbt_fpga/modules/gbt-fpga/

remove_files -fileset sources_1 [glob $script_path/src/*.sv] 
remove_files -fileset sources_1 [glob $script_path/src/*.vhd] 
remove_files -fileset sources_1 [glob $gbt_location/example_designs/core_sources/exampleDsgn_package.vhd] 
remove_files -fileset sources_1 [glob $gbt_location/example_designs/core_sources/gbt_bank_reset.vhd] 

## for debugging
set gbt_ex_core_sources $gbt_location/example_designs/core_sources 
remove_files -fileset sources_1 [glob $gbt_ex_core_sources/rxframeclk_phalgnr/gbt_rx_frameclk_phalgnr.vhd] 
remove_files -fileset sources_1 [glob $gbt_ex_core_sources/gbt_pattern_checker.vhd] 
remove_files -fileset sources_1 [glob $gbt_ex_core_sources/gbt_pattern_generator.vhd] 
remove_files -fileset sources_1 [glob $gbt_ex_core_sources/gbt_pattern_matchflag.vhd] 

remove_files -fileset sources_1 [get_files gbt_pll40m.xci]
source $script_path/../modules/Zynq_USplus_gbt_fpga/unload.tcl  
