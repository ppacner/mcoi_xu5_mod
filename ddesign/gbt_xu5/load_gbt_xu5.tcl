 
set script_path [file dirname [info script]]

# create transceiver for xu5
set gbt_location $script_path/../modules/Zynq_USplus_gbt_fpga/modules/gbt-fpga/
add_files -fileset sources_1 [glob $gbt_location/example_designs/core_sources/exampleDsgn_package.vhd] 
add_files -fileset sources_1 [glob $gbt_location/example_designs/core_sources/gbt_bank_reset.vhd] 

## for debugging
set gbt_ex_core_sources $gbt_location/example_designs/core_sources 
add_files -fileset sources_1 [glob $gbt_ex_core_sources/rxframeclk_phalgnr/gbt_rx_frameclk_phalgnr.vhd] 
add_files -fileset sources_1 [glob $gbt_ex_core_sources/gbt_pattern_checker.vhd] 
add_files -fileset sources_1 [glob $gbt_ex_core_sources/gbt_pattern_generator.vhd] 
add_files -fileset sources_1 [glob $gbt_ex_core_sources/gbt_pattern_matchflag.vhd] 

# generate pll 40mhz
source $script_path/pll_40m/gbt_pll_clk40m.tcl

# gbt wrapper for xu5 module
add_files -fileset sources_1 [glob $script_path/src/gbt_xu5.vhd] 
add_files -fileset sources_1 [glob $script_path/src/gbt_pingroup.sv] 
add_files -fileset sources_1 [glob $script_path/src/gbt_xu5_system.sv] 

## generate GBT block
source $script_path/../modules/Zynq_USplus_gbt_fpga/load2project.tcl  

