//----------------------------------------------------------------------------//
//Filename: xu5pl_app.sv
//Description: 1st version of firmware for xu5 based mcoi
//Author: Petr Pacner
//Created On: 2020-03-13 Fr 10:08  
//----------------------------------------------------------------------------//

module gbt_xu5_system(
    //api
    output [83:0] gbt_data_o,
    input [83:0] gbt_data_i,
    input clk_40m,
    input mgt_clk_120m,

    //status
    input sfp_los_i,
    output sfp_rats_o,
    output sfp_txdis_o,

    //HW pins
    input sfp_rx_p,
    input sfp_rx_n,
    output sfp_tx_p,
    output sfp_tx_n

);
    //status 
    assign sfp_rats_o = 1'b0;
    assign sfp_txdis_o = 1'b1;
    
	// GBT instance
	gbt_xu5 gbt_xu5_inst(

		//clock
		.frameclk_40mhz(clk_40m),
		.xcvrclk(mgt_clk_120m),
        .rx_frameclk_o(),
        .rx_wordclk_o(),
        .tx_frameclk_o(),
        .tx_wordclk_o(),
        .rx_frameclk_rdy_o(),

		// reset
		.gbtbank_general_reset_i(1'b0),
		.gbtbank_manual_reset_tx_i(1'b0),
		.gbtbank_manual_reset_rx_i(1'b0),

		// gbt transceiver inouts
		.gbtbank_mgt_rx_p(sfp_rx_p),
		.gbtbank_mgt_rx_n(sfp_rx_n),
		.gbtbank_mgt_tx_p(sfp_tx_p),
		.gbtbank_mgt_tx_n(sfp_tx_n),

		// data
		.gbtbank_gbt_data_i(gbt_data_i),
		.gbtbank_wb_data_i('0),
        .tx_data_o(),
        .wb_data_o(),

		.gbtbank_gbt_data_o(gbt_data_o),
		.gbtbank_wb_data_o(),

		// reconf.
		.gbtbank_mgt_drp_rst(1'b0),
		.gbtbank_mgt_drp_clk(1'b0), //connected to 125Mhz

		// tx ctrl
		.tx_encoding_sel_i(1'b0),
		.gbtbank_tx_isdata_sel_i(1'b0),
        .gbtbank_test_pattern_sel_i(2'b11),

		// rx ctrl
		.rx_encoding_sel_i(1'b0),
		.gbtbank_reset_gbtrxready_lost_flag_i(sfp_los_i), // ???
		.gbtbank_reset_data_errorseen_flag_i('0),
        .gbtbank_rxframeclk_alignpatter_i(3'b000),
		.gbtbank_rxbitslit_rstoneven_i(1'b1),

		// tx status
		.gbtbank_link_ready_o(),
		.gbtbank_tx_aligned_o(),
		.gbtbank_tx_aligncomputed_o(),

		// rx status
		.gbtbank_gbttx_ready_o(),
		.gbtbank_gbtrx_ready_o(),
		.gbtbank_gbtrxready_lost_flag_o(),
		.gbtbank_rxdata_errorseen_flag_o(),
		.gbtbank_rxextradata_widebus_errorseen_flag_o(),
		.gbtbank_rx_isdata_sel_o(),
		.gbtbank_rx_errordetected_o(),
		.gbtbank_rx_bitmodified_flag_o(),
		.gbtbank_rxbitslip_rst_cnt_o(),

		//xcvr ctrl
		.gbtbank_loopback_i(3'b000),
		.gbtbank_tx_pol(1'b1),
		.gbtbank_rx_pol(1'b1)); 

endmodule
