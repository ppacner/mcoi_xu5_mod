
module gbt_pingroup(
    //gbt signals
    input sfp_rx_p,
    input sfp_rx_n,
    output sfp_tx_p,
    output sfp_tx_n,
    output sfp_rats_o,
    output sfp_txdis_o,
    input sfp_los_i,
    input mgt_clk120m_ip,
    input mgt_clk120m_in,
    output clk40m_o,
    
    //api
    input [83:0] gbt_data_i,
    output [83:0] gbt_data_o
);

logic clk120m_fromgte4;
logic mgt_clk120m;

IBUFDS_GTE4 #(
      .REFCLK_EN_TX_PATH(1'b0),   
      .REFCLK_HROW_CK_SEL(2'b00),
      .REFCLK_ICNTL_RX(2'b00) 
   )
   ibufds_gte4_i (
       .O(mgt_clk120m),         
       .ODIV2(clk120m_fromgte4), 
       .CEB(1'b0),     
       .I(mgt_clk120m_ip),         
       .IB(mgt_clk120m_in) 
   );

logic clk120m;
BUFG_GT ibuf_txpll_i (
   .O(clk120m),
   .CE(1'b1),
   .CEMASK(1'b0),
   .CLR(1'b0),
   .CLRMASK(1'b0),
   .DIV(3'b000),
   .I(clk120m_fromgte4)
);

//pll 120m -> 40mhz
logic clk40m_from_pll;
gbt_pll40m gbt_pll40m_i (
    .clk120m_i(clk120m),
    .clk40m_o(clk40m_from_pll),
    .reset(0),
    .locked()
);

assign clk40m_o = clk40m_from_pll;

gbt_xu5_system gbtxu5_sys_i (
    .gbt_data_o(gbt_data_o),
    .gbt_data_i(gbt_data_i),
    .clk_40m(clk40m_from_pll),
    .mgt_clk_120m(mgt_clk120m),

    .sfp_los_i(sfp_los_i),
    .sfp_rats_o(sfp_rats_o),
    .sfp_txdis_o(sfp_txdis_o),

    //HW pins
    .sfp_rx_p(sfp_rx_p),
    .sfp_rx_n(sfp_rx_n),
    .sfp_tx_p(sfp_tx_p),
    .sfp_tx_n(sfp_tx_n)
);

endmodule
