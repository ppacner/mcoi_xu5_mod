
`timescale 1ns/100ps 

module gbt_xu5_top(
    input sfp1_gbitin_p,
    input sfp1_gbitin_n,
    output sfp1_gbitout_p,
    output sfp1_gbitout_n,
    input sfp1_los,
    output sfp1_rateselect,
    output sfp1_txdisable,
    input mgt_clk_p,
    input mgt_clk_n,
    output clk_out
);

gbt_pingroup gbt_pingroup_i(
    .sfp_rx_p(sfp1_gbitin_p),
    .sfp_rx_n(sfp1_gbitin_n),
    .sfp_tx_p(sfp1_gbitout_p),
    .sfp_tx_n(sfp1_gbitout_n),
    .sfp_rats_o(sfp1_rateselect),
    .sfp_txdis_o(sfp1_txdisable),
    .sfp_los_i(sfp1_los),
    .mgt_clk120m_ip(mgt_clk_p),
    .mgt_clk120m_in(mgt_clk_n),
    .clk40m_o(clk_out),
    .gbt_data_i(80'h000BABEAC1DACDCFFFFF),
    .gbt_data_o());

endmodule
