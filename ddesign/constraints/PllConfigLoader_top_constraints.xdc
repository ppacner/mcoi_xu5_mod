
#DEBUG LEDS
set_property PACKAGE_PIN H2 [get_ports {dled[0]}]
set_property PACKAGE_PIN P9 [get_ports {dled[1]}]
set_property PACKAGE_PIN K5 [get_ports {dled[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {dled}]

set_property PACKAGE_PIN E10 [get_ports btn]
set_property IOSTANDARD LVCMOS18 [get_ports btn]

set_property PACKAGE_PIN AA13 [get_ports clk_out]
set_property IOSTANDARD LVCMOS18 [get_ports clk_out]

set_property PACKAGE_PIN AD14 [get_ports i2c_scl]
set_property IOSTANDARD LVCMOS18 [get_ports i2c_scl]

set_property PACKAGE_PIN AD15 [get_ports i2c_sda]
set_property IOSTANDARD LVCMOS18 [get_ports i2c_sda]
