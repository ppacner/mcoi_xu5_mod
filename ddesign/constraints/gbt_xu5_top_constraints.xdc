set_property PACKAGE_PIN Y1 [get_ports sfp1_gbitin_n]
set_property PACKAGE_PIN Y2 [get_ports sfp1_gbitin_p]
set_property PACKAGE_PIN W3 [get_ports sfp1_gbitout_n]
set_property PACKAGE_PIN W4 [get_ports sfp1_gbitout_p]

set_property PACKAGE_PIN F1 [get_ports sfp1_los]
set_property IOSTANDARD LVCMOS18 [get_ports sfp1_los]

set_property PACKAGE_PIN C4 [get_ports sfp1_rateselect]
set_property IOSTANDARD LVCMOS18 [get_ports sfp1_rateselect]

set_property PACKAGE_PIN G1 [get_ports sfp1_txdisable]
set_property IOSTANDARD LVCMOS18 [get_ports sfp1_txdisable]

set_property PACKAGE_PIN AA13 [get_ports clk_out]
set_property IOSTANDARD LVCMOS18 [get_ports clk_out]

set_property PACKAGE_PIN Y5 [get_ports mgt_clk_n]
set_property PACKAGE_PIN Y6 [get_ports mgt_clk_p]
create_clock -period 8.333 -name mgt_clk [get_ports mgt_clk_p]
