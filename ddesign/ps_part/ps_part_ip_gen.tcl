#------------------------------------------------------------------------------#
# Petr Pacner | CERN | 2020-01-13 Mo 10:37   
# 
# GENERATE PROCESSING PART
#------------------------------------------------------------------------------#

## GENERATE IPs

# set name of generated ip core
set ip_root_name zynq_ultrasp_ps 

#quotation mark at the and -> string result without space inbetween
set ip_orig_name [join [list $ip_root_name "_0"] ""]

if {[lsearch -exact [get_ips] $ip_orig_name] >= 0} {
	puts "the ip core already exists"
	#read_ip [get_files [join [list $ip_orig_name ".xci"] ""]]

} else {
	create_ip -name zynq_ultra_ps_e\
		-vendor xilinx.com\
		-library ip\
		-version 3.3\
		-module_name $ip_orig_name

	set_property -dict [list CONFIG.PSU__ENET0__PERIPHERAL__ENABLE {1}\
		CONFIG.PSU__GPIO0_MIO__PERIPHERAL__ENABLE {1}\
		CONFIG.PSU__GPIO1_MIO__PERIPHERAL__ENABLE {1}\
		CONFIG.PSU__I2C0__PERIPHERAL__ENABLE {1}\
		CONFIG.PSU__I2C0__PERIPHERAL__IO {MIO 10 .. 11}\
		CONFIG.PSU__USB0__REF_CLK_SEL {Ref Clk2}\
		CONFIG.PSU__USB0__REF_CLK_FREQ {100}\
		CONFIG.PSU__TTC0__PERIPHERAL__ENABLE {1}\
		CONFIG.PSU__DDRC__CWL {12}\
		CONFIG.PSU__DDRC__BG_ADDR_COUNT {1}\
		CONFIG.PSU__DDRC__DEVICE_CAPACITY {4096 MBits}\
		CONFIG.PSU__DDRC__DRAM_WIDTH {16 Bits}\
		CONFIG.PSU__DDRC__ECC {Enabled}\
		CONFIG.PSU__DDRC__ROW_ADDR_COUNT {15}\
		CONFIG.PSU__DDRC__SPEED_BIN {DDR4_2400T}\
		CONFIG.PSU__UART0__PERIPHERAL__ENABLE {1}\
		CONFIG.PSU__UART0__PERIPHERAL__IO {MIO 38 .. 39}\
		CONFIG.PSU__USB0__PERIPHERAL__ENABLE {1}\
		CONFIG.PSU__USB3_0__PERIPHERAL__ENABLE {1}\
		CONFIG.PSU__USB3_0__PERIPHERAL__IO {GT Lane2}\
		CONFIG.PSU__USE__M_AXI_GP0 {1}\
		CONFIG.PSU_MIO_7_INPUT_TYPE {schmitt}\
		CONFIG.PSU_MIO_12_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_12_INPUT_TYPE {schmitt}\
		CONFIG.PSU_MIO_26_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_26_SLEW {slow}\
		CONFIG.PSU_MIO_27_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_27_SLEW {slow}\
		CONFIG.PSU_MIO_28_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_28_SLEW {slow}\
		CONFIG.PSU_MIO_29_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_29_SLEW {slow}\
		CONFIG.PSU_MIO_30_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_30_SLEW {slow}\
		CONFIG.PSU_MIO_31_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_31_SLEW {slow}\
		CONFIG.PSU_MIO_32_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_33_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_34_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_35_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_36_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_37_PULLUPDOWN {disable}\
		CONFIG.PSU_MIO_45_SLEW {slow}] [get_ips $ip_orig_name]

	# generate ip
	generate_target all [get_ips]
}

## MAKE CONNECTIONs

