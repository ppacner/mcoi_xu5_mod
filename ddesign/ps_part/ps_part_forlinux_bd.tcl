#------------------------------------------------------------------------------#
# Petr Pacner | CERN | 2020-01-13 Mo 10:37   
# 
# GENERATE PROCESSING PART
#------------------------------------------------------------------------------#

## GENERATE IPs

# set name of generated ip core
set ip_root_name zynq_ultrasp_ps 
set ip_orig_name [join [list $ip_root_name "_0"] ""]

startgroup
    create_bd_cell -type ip\
                   -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.3 $ip_orig_name 
    set_property -dict [list CONFIG.PSU__ENET0__PERIPHERAL__ENABLE {1}\
                             CONFIG.PSU_BANK_0_IO_STANDARD {LVCMOS18}\
                             CONFIG.PSU_BANK_1_IO_STANDARD {LVCMOS18}\
                             CONFIG.PSU_BANK_2_IO_STANDARD {LVCMOS18}\
                             CONFIG.PSU_BANK_3_IO_STANDARD {LVCMOS18}\
                             CONFIG.PSU__GPIO_EMIO__PERIPHERAL__ENABLE {0}\
                             CONFIG.PSU__I2C0__PERIPHERAL__ENABLE {1}\
                             CONFIG.PSU__I2C0__PERIPHERAL__IO {MIO 10 .. 11}\
                             CONFIG.PSU__PJTAG__PERIPHERAL__ENABLE {0}\
                             CONFIG.PSU__UART0__PERIPHERAL__ENABLE {1}\
                             CONFIG.PSU__UART0__PERIPHERAL__IO {MIO 38 .. 39}\
                             CONFIG.PSU__ENET0__PERIPHERAL__ENABLE {1}\
                             CONFIG.PSU__ENET0__GRP_MDIO__ENABLE {1}\
                             CONFIG.PSU__DDRC__CWL {12}\
                             CONFIG.PSU__QSPI__PERIPHERAL__ENABLE {1}\
                             CONFIG.PSU__QSPI__PERIPHERAL__DATA_MODE {x4}\
                             CONFIG.PSU__USE__M_AXI_GP2 {0}\
                             CONFIG.PSU__USE__FABRIC__RST {0}\
                             CONFIG.PSU__FPGA_PL0_ENABLE {0}\
                             CONFIG.PSU__DDRC__DEVICE_CAPACITY {4096 MBits}\
                             CONFIG.PSU__FPGA_PL0_ENABLE {0}\
                             CONFIG.PSU__DDRC__DRAM_WIDTH {16 Bits}\
                             CONFIG.PSU__DDRC__BG_ADDR_COUNT {1}\
                             CONFIG.PSU__DDRC__ECC {Enabled}\
                             CONFIG.PSU__DDRC__ROW_ADDR_COUNT {15}\
                             CONFIG.PSU__DDRC__SPEED_BIN {DDR4_2400T}\
                             CONFIG.PSU__CRF_APB__DDR_CTRL__FREQMHZ {1200}
                             CONFIG.PSU__FPGA_PL0_ENABLE {1}\
                             CONFIG.PSU__CRL_APB__PL0_REF_CTRL__FREQMHZ {20}]\
                             [get_bd_cells zynq_ultrasp_ps_0]
endgroup

