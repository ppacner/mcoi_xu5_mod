## LOADING OF THE BITSTREAM TO QSPI FLASH
* Generate bitstream
* Export hardware with bitstream (click in Vivado File -> Export -> Export Hardware)
* Check box "include bitstream" -> click OK
* File -> Launch SDK -> click OK
* First create fsbl (File -> New -> Application Project)
* Write desired name of the project and press "next"
* Than choose option "Zynq MP FSBL" and press "finish"
* Create the new application project and in the second step choose empty template
* Create image (Xilinx -> Create Boot Image)
* The both files should be part of the image

## loading design to qspi
[loading desing to qspi](https://forums.xilinx.com/t5/Design-and-Debug-Techniques-Blog/Booting-a-PL-design-from-a-PS-attached-SPI-flash-in-Zynq/ba-p/1015804)

## OTHERS
* in user manual from enclustra is written, that the JTAG mode is not implemented
for XU5 module, but flashing to the QSPI memory works anyway
* it can happen sometimes that the console will output this message, that if the
flashing procedure fails you should switch the device to JTAG mode, in that case
try create new fsbl and "c" project application project

* on the devkit you have to set the switches:
CFG A = [1: OFF, 2: OFF, 3:OFF, 4: ON] Set CFG B = [1: OFF, 2: ON, 3: ON, 4: OFF]

## TODO
* find the mistake in generated Zynq IP core
