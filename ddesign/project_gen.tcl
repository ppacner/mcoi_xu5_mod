
set base_name mcoi 
set module_name xu5  
set device_name xczu4ev-sfvc784-1-i

# variables 
set OPTICAL_INTERFACE 1
set proj_name ${base_name}_${module_name}
set proj_wrap [join [list $proj_name "_w"] ""]
set proj_dest [exec pwd] 
set proj_path [join [list $proj_dest "/Vivado_project/"] ""]
set proj_bi_cores $proj_dest/modules/BI_HDL_Cores/cores_for_synthesis/

# create project
if {!([lindex [get_projects] 0] == $proj_name)} {
    create_project -force ${proj_name} ${proj_path} -part ${device_name} 
    set_property target_language Verilog [current_project]
}

# load Top
add_files -fileset sources_1 [glob $proj_dest/mcoi_xu5_top.sv] 
add_files -fileset sources_1 [glob $proj_dest/mcoi_xu5_top_w.sv] 

# load constraints
add_files -fileset constrs_1 [glob $proj_dest/constraints/*.xdc] 

# DEBUG of single parts
add_files -fileset sources_1 [glob $proj_dest/mcoi_diag/hw_test/*.sv] 
add_files -fileset sources_1 [glob $proj_dest/mcoi_diag/PllConfig/hw_test/*.sv] 
add_files -fileset sources_1 [glob $proj_dest/mcoi_diag/I2cDev/hw_test/*.sv] 
add_files -fileset sources_1 [glob $proj_dest/gbt_xu5/hw_test/*.sv] 
add_files -fileset sources_1 [glob $proj_dest/qspi_load_test/*.sv] 

# DEBUG constraints for single parts of design set as disabled
set_property is_enabled false [get_files I2cDev_top_constraints.xdc]
set_property is_enabled false [get_files PllConfigLoader_top_constraints.xdc]
set_property is_enabled false [get_files gbt_xu5_top_constraints.xdc]
set_property is_enabled false [get_files qspi_load_test_constraints.xdc]

# load base
source $proj_dest/mcoi_base/load_base.tcl

# load diagnostics 
source $proj_dest/mcoi_diag/load_diagnostics.tcl

if {$OPTICAL_INTERFACE} {
    # load gbt_xu5 block
    source $proj_dest/gbt_xu5/load_gbt_xu5.tcl 

    # create block design
    source $proj_dest/bd_design/mcoi_xu5_optics.tcl
    
} else {
    # TODO create design without optical interface
    #unload gbt_xu5 block
    source $proj_dest/gbt_xu5/unload_gbt_xu5.tcl
}

#set project top file
update_compile_order -fileset sources_1
set_property source_mgmt_mode DisplayOnly [current_project]
