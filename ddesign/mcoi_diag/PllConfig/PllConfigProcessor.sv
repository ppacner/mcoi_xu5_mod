`timescale 1ns/100ps 

// descriptions:
// bram rom has to be set follows: out data width [18:0], address [8:0]
// data word structure {flag[2:0], reg[7:0], val/mask[7:0]}
// wait data word sructure [0x04, wait_cycles_cnt[15:0]]
//
// flags: 0x0 - copy
//        0x1 - set
//        0x2 - combined
//        0x3 - read
//        0x4 - validate
//        0x5 - wait
//        0x6 - clear
//        0x7 - End of memory

module PllConfigProcessor(
    input clk,
    input rstp,
    output done_o,

    //rom signals
    input [18:0] rom_data_i,
    output [9:0] rom_addr_o,

    //i2c reader signals
    output rw_o,
    output valid_o,
    input ready_i,
    input [7:0] data_i,
    output [15:0] data_o
);

localparam CLR = 3'h6;
localparam SET = 3'h1;
localparam COPY = 3'h0;
localparam COMB = 3'h2;
localparam EOFM = 3'h7;

localparam TRUE = 1'b1;
localparam FALSE = 1'b0;

localparam IDLE = 3'b000;
localparam MEMR = 3'b001; //memory read
localparam WRIT = 3'b010; //put data
localparam READ = 3'b011; //get data
localparam VALD = 3'b100;
localparam WAIT = 3'b101;
localparam ADDR = 3'b110;
localparam WFRD = 3'b111; // wait for rom data  

logic [2:0] flag;
logic [7:0] ireg, ival;
assign flag = rom_data_i[18:16];
assign ireg = rom_data_i[7:0];
assign ival = rom_data_i[15:8]; //could be value or mask depends on flag number

logic [2:0] state, next;
always_ff@(posedge clk or posedge rstp) begin
    if(rstp) state <= IDLE;
    else state <= next; end

logic [1:0] delay; //delay of 4 clk
logic ready_cc, ok, done;
logic [15:0] data_to_bus, timeout;

always_comb begin
    next = state;
    case(state)
        IDLE: next = WFRD;
        MEMR: begin if(flag == EOFM) next = IDLE;
                    else if(flag == COPY 
                         || flag == CLR 
                         || flag == SET 
                         || flag == COMB) next = WRIT;
                    else next = flag; end  
        WRIT: if(ready_i && !ready_cc) next = ADDR;  
        READ: if(ready_i && !ready_cc) next = (flag != VALD) ? ADDR : VALD;
        VALD: next = (ok) ? ADDR : WFRD;
        WAIT: if(timeout == 0) next = ADDR;
        ADDR: next = WFRD;
        WFRD: if(delay == 2'd3) next = MEMR;
    endcase end

logic rw, valid; //last variable indicates end of fsm cycle
logic [9:0] addr;
logic [15:0] shift_reg;
always_ff@(posedge clk or posedge rstp) begin
    ready_cc <= ready_i;
    shift_reg <= shift_reg;
    timeout <= timeout;
    addr <= addr;
    done <= FALSE;
    rw <= FALSE;
    valid <= FALSE;
    ok <= ok;

    if(rstp) begin rw <= FALSE;
                valid <= FALSE;
                done <= TRUE;
                shift_reg <= '0;
                timeout <= '0;
                data_to_bus <= '0;
                addr <= '0;
                delay <= '0;
                ok <= FALSE;
    end else begin case(state)
        IDLE: begin rw <= FALSE;
                 valid <= FALSE;
                 timeout <= '0;
                 delay <= '0;
                 data_to_bus <= '0;
                 done <= TRUE;
                  addr <= '0; end
        MEMR: begin if(flag == EOFM) done <= TRUE; 
                    else if(flag == WAIT) timeout <= {ival, ireg}; end
        WRIT: begin if(flag == COPY) data_to_bus <= {shift_reg[7:0] | (shift_reg[15:8] & ival), ireg};
                    else if(flag == SET && flag == COMB) data_to_bus <= {shift_reg[7:0] | ival, ireg};
                    else if(flag == CLR) data_to_bus <= {shift_reg[7:0] & ~ival, ireg};
                    else data_to_bus <= {ival, ireg}; 
                    if(ready_i && next == state) valid <= TRUE; end
        READ: begin rw <= TRUE;
                    data_to_bus <= {8'hxx, ireg};
                    if(ready_i && next == state) valid <= TRUE;
                    else if(ready_i && !ready_cc) 
                        shift_reg <= {shift_reg[7:0], data_i & ~ival}; end
        VALD: begin if(shift_reg[7:0] == ival) ok <= TRUE;
                    else if(state == next) addr <= addr - 1; end 
        WAIT: if(state == next) timeout <= timeout - 1;
        ADDR: begin addr <= addr + 1;
                      ok <= FALSE; end
        WFRD: begin if(delay == 2'd3 && state == next) delay <= '0;
              else delay <= delay + 1; end
     endcase end end

assign rw_o = rw; 
assign done_o = done;
assign valid_o = valid;
assign data_o = data_to_bus;
assign rom_addr_o = addr;

endmodule
