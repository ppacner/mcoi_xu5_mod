
`timescale 1ns/100ps 

module PllConfigLoader(
    input clk,
    input rstp,
    output valid_o,
    output rw_o,
    input ready_i,
    input [7:0] byte_i,
    output [15:0] byte_o,
    output fin_o
);

logic [18:0] memory_value;
logic [9:0] memory_addr;

PllConfigProcessor pll_config_psu(
    .clk(clk),
    .rstp(rstp),
    .done_o(fin_o),

    .rom_data_i(memory_value),
    .rom_addr_o(memory_addr),

    .rw_o(rw_o),
    .valid_o(valid_o),
    .ready_i(ready_i),
    .data_i(byte_i),
    .data_o(byte_o));

pll_config_rom config_memory(
  .clka(clk),
  .addra(memory_addr), 
  .douta(memory_value));

endmodule
