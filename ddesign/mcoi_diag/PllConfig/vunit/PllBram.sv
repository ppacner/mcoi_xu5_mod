`timescale 1ns/100ps 

module pll_loader(
   input clk,
   input rstp,
   output [26:0] out_data 
);

logic [8:0] cnt;
logic [2:0] delay;
always_ff@(posedge clk or posedge rstp) begin
    cnt <= cnt;
    if(rstp) cnt <= '0;  
    else if(delay == 4) cnt <= cnt + 1; end

always_ff@(posedge clk or posedge rstp) begin
    if(rstp) delay <= '0;
    else begin
        if(delay == 4) delay <= '0;
        else delay <= delay + 1; end end

pll_config_rom pll_config_rom_i(
  .clka(clk),    
  .addra(cnt),  
  .douta(out_data));

endmodule
