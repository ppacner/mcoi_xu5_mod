`timescale 1ns/100ps 

module pll_loader_sim;

logic clk, rstp;
logic [26:0] data_from_memory;

always #10 clk = ~clk; 

initial begin 
    clk = 1'b0;
    rstp = 1'b1;
    repeat(10) #10 clk = ~clk;
    rstp = 1'b0;
    //#500 $finish; 
end

pll_loader pll_loader_i(
    .clk(clk),
    .rstp(rstp),
    .out_data(data_from_memory)
);

//always@(posedge clk) $display("%d", data_from_memory);
always@(posedge clk) if(data_from_memory == 27'h7000000) $stop;

endmodule
