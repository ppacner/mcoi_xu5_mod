
`timescale 1ns/100ps 

module PllConfigLoader_top (
    input btn,
    output clk_out,
    output [2:0] dled,

    inout i2c_sda,
    inout i2c_scl,
    input clk100m_pl_p,
    input clk100m_pl_n,

    input mgt_clk_p,
    input mgt_clk_n
);
// TESTING NOTES
// 1) try to connect pl_clk0 to output and check it on oscilloscope
// 2) try to programe the pll and check it on oscilloscope (level shifter)
// freq calculus: 


logic clk120m_fromgte4, clk100m_ref, clk120m;
assign clk_out = clk100m_ref;

IBUFDS_GTE4 #(
      .REFCLK_EN_TX_PATH(1'b0),   
      .REFCLK_HROW_CK_SEL(2'b00),
      .REFCLK_ICNTL_RX(2'b00) 
   )
   ibufds_gte4_i (
       .O(),         
       .ODIV2(clk120m_fromgte4), 
       .CEB(1'b0),     
       .I(mgt_clk_p),         
       .IB(mgt_clk_n) 
   );

BUFG_GT ibuf_txpll_i (
   .O(clk120m),
   .CE(1'b1),
   .CEMASK(1'b0),
   .CLR(1'b0),
   .CLRMASK(1'b0),
   .DIV(3'b000),
   .I(clk120m_fromgte4)
);

IBUFDS ibufds_i(
.O(clk100m_ref),
.I(clk100m_pl_p),
.IB(clk100m_pl_n));

logic switch, fin, fin_cc;
always@(posedge clk100m_ref) begin
    fin_cc <= fin;
    if(btn) switch <= 1'b0;
    else begin switch <= 1'b0;  
         if(fin && !fin_cc) switch <= 1'b1; end end

logic valid, rd, rw;
logic [7:0] byte_from_reader;
logic [15:0] byte_to_reader;
   PllConfigLoader PllConfigLoader_i(
       .clk(clk100m_ref),
       .rstp(switch),
       .valid_o(valid),
       .rw_o(rw),
       .ready_i(rd),
       .byte_i(byte_from_reader),
       .byte_o(byte_to_reader),
       .fin_o(fin));

logic done, start, stop, ack_r, ack_s, sendb, getb; 
logic [7:0] byte_to_i2c, byte_from_i2c;

I2cReader i2c_reader_i(
    .clk(clk100m_ref),
    .rstp(1'b0),

    .Done_i(done),
    .AckReceived_i(ack_r),
    .Byte_ib8(byte_from_i2c),
    .SendStartBit_op(start),
    .SendStopBit_op(stop),
    .SendByte_op(sendb),
    .GetByte_op(getb),
    .AckToSend_o(ack_s),
    .Byte_ob8(byte_to_i2c),

    .valid_i(valid),
    .rw_i(rw),
    .ready_o(rd),

    .data_o(byte_from_reader),
    .data_i(byte_to_reader));

I2cMasterGeneric i2c_master_i(
    .Clk_ik(clk100m_ref),
    .Rst_irq(1'b0),
    .SendStartBit_ip(start),
    .SendByte_ip(sendb),
    .GetByte_ip(getb),
    .SendStopBit_ip(stop),
    .Byte_ib8(byte_to_i2c),
    .AckToSend_i(ack_s),
    .Byte_ob8(byte_from_i2c),
    .AckReceived_o(ack_r),
    .Done_o(done),

    .Scl_ioz(i2c_scl),
    .Sda_ioz(i2c_sda));

mcoi_xu5_optics mcoi_xu5_optics_i();

assign dled[0] = done;
assign dled[1] = fin;
assign dled[2] = switch;

endmodule
