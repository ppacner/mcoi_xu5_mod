import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, FallingEdge 


def copy_register_stamp(reg0, reg1, mask):
    out = []
    out.append(read_reg(reg0, 0x00))
    out.append(read_reg(reg1, 0x00))
    out.append(0x0*(2**16) + mask*(2**8) + reg0)
    return out

def set_register_stamp(reg, mask):
    out = []
    out.append(read_reg(reg, mask))
    out.append(0x1*(2**16) + mask*(2**8) + reg)
    return out

def combined_register_stamp(reg, val, mask):
    out = []
    out.append(read_reg(reg, mask))
    out.append(0x2*(2**16) + (val & mask)*(2**8) + reg)
    return out

def read_reg(reg, mask):
    return 0x3*(2**16) + mask*(2**8) + reg

def validate_register_stamp(reg, mask, value):
    out = []
    out.append(read_reg(reg, mask))
    out.append(0x4*(2**16) + value*(2**8) + reg)
    return  out

def wait_stamp(time):
    flag = 0x5 
    return flag*(2**16) + time

def clr_register_stamp(reg, mask):
    out = []
    out.append(read_reg(reg, 0x00))
    out.append(0x6*(2**16) + mask*(2**8) + reg)
    return out

def stop_stamp():
    return 0x7*(2**16)

def create_memory_map():
    """
    19-bit numbers in cells
    """
    out = []
    out.extend(set_register_stamp(1, 0xff))
    out.extend(clr_register_stamp(2, 0xff))
    out.extend(copy_register_stamp(3, 4, 0xff))
    out.extend(combined_register_stamp(5, 2, 0xff))
    out.extend(clr_register_stamp(6, 0xff))
    out.extend(validate_register_stamp(7, 0x00, 0xff))
    out.append(wait_stamp(10))
    out.append(stop_stamp())
    return out

@cocotb.coroutine
def i2c_reader_simul(dut):
    dut.ready_i <= 1
    while(1):
        yield RisingEdge(dut.valid_o)
        yield RisingEdge(dut.clk)
        dut.ready_i <= 0
        for i in range(0, 10):
            yield RisingEdge(dut.clk)
        dut.ready_i <= 1
        yield RisingEdge(dut.clk)

@cocotb.test()
async def pll_config_tb(dut):
    clock = Clock(dut.clk, 1, units="ns")
    cocotb.fork(clock.start())  
    cocotb.fork(i2c_reader_simul(dut))

    emulated_rom = create_memory_map()
    print(emulated_rom)

    dut.rstp <= 1
    await RisingEdge(dut.clk)
    dut.rom_data_i <= emulated_rom[0]
    dut.data_i <= 0xff;
    dut.rstp <= 0
    await FallingEdge(dut.done_o)

    # for i in range(0, 12):
    while(not int(dut.done_o)):
        await RisingEdge(dut.clk) # delay 1 clk
        addr = int(dut.rom_addr_o)
        dut.rom_data_i <= emulated_rom[addr]
        print(int(dut.rom_addr_o))
        # print(dut.state)

        # if(dut.valid_o):
            # print(int(dut.rom_addr_o))
            # # print(int(dut.rom_data_i) & 0xffff00//(2**8), int(dut.data_o))
            # dut.rom_data_i <= emulated_rom[int(dut.rom_addr_o)]
            # await RisingEdge(dut.ready_i)

    print("End of memory")
