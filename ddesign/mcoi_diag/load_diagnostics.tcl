
set script_path [file dirname [info script]]
set bi_hdl_cores $script_path/../modules/BI_HDL_Cores/cores_for_synthesis/

# load diagnostics blocks
add_files -fileset sources_1 [glob $script_path/mcoi_diagnostics.sv] 
add_files -fileset sources_1 [glob $script_path/I2cDev/I2cDev.sv] 
add_files -fileset sources_1 [glob $script_path/I2cReader/I2cReader.sv] 
add_files -fileset sources_1 [glob $script_path/PllConfig/*.sv] 
add_files -fileset sources_1 [glob $script_path/Rs232/*.v] 

# load from general modules
add_files -fileset sources_1 [glob $bi_hdl_cores/I2cMasterGeneric.v] 

##TODO add UART block

# generate pll config rom
source $script_path/PllConfig/config_rom/pll_config_rom.tcl
