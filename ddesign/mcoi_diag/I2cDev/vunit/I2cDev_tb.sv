
`timescale 1ns/100ps 

module I2cDev_tb();

logic clk, rstp, valid, rw, rd;
logic [7:0] byte_to_device, byte_from_device;

initial begin clk = 1'b0;
              rstp = 1'b1;
              repeat(10) #10 clk = ~clk;
              rstp = 1'b0; 
              forever #10 clk = ~clk; end

always begin
    @(posedge valid);
    @(posedge clk);
    rd = 1'b0;
    $display("got valid from slave");
    repeat(10) #10 clk = ~clk;
    rd = 1'b1;
    $display("got valid from slave"); end

initial begin @(posedge rd);
              $finish; end

I2cDev i2cdev_i(
    .rstp(rstp),
    .clk(clk),
    .valid_o(valid),
    .rw_o(rw),
    .ready_i(rd),
    .byte_i(byte_from_device),
    .byte_o(byte_to_device),
    .data_o(dev_data),
    .fin_o(fin));

//TODO write the oposite part

endmodule
