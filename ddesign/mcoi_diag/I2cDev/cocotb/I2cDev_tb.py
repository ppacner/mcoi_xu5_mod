import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, FallingEdge 

@cocotb.coroutine
def i2c_reader_simul(dut):
    dut.ready_i <= 1
    while(1):
        yield RisingEdge(dut.valid_o)
        yield RisingEdge(dut.clk)
        dut.ready_i <= 0
        for i in range(0, 10):
            yield RisingEdge(dut.clk)
        dut.ready_i <= 1
        yield RisingEdge(dut.clk)

@cocotb.test()
async def pll_config_tb(dut):
    clock = Clock(dut.clk, 1, units="ns")
    cocotb.fork(clock.start())  
    cocotb.fork(i2c_reader_simul(dut))

    dut.rstp <= 1
    await RisingEdge(dut.clk)
    dut.byte_i <= 0xff;
    dut.rstp <= 0
    await FallingEdge(dut.fin_o)
    await RisingEdge(dut.fin_o)
