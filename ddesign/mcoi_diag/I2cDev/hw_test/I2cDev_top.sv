
`timescale 1ns/100ps 

module I2cDev_top(
    input btn,
    output [2:0] dled,

    input clk100m_pl_p, 
    input clk100m_pl_n, 

    inout i2c_sda,
    inout i2c_scl
);

logic clk100m_ref;
IBUFDS ibufds_i(
.O(clk100m_ref),
.I(clk100m_pl_p),
.IB(clk100m_pl_n));

logic rw, rd, valid;
logic [7:0] byte_from_reader;
logic [15:0] byte_to_reader;
I2cDev #(.OUT_SIZE(4)) 
i2cdev_i(
    .rstp(btn),
    .clk(clk100m_ref),
    .valid_o(valid),
    .rw_o(rw),
    .ready_i(rd),
    .byte_i(byte_from_reader),
    .byte_o(byte_to_reader),
    .data_o(),
    .fin_o(dled[0])); //led should blink

logic done, start, stop, ack_r, ack_s, sendb, getb; 
logic [7:0] byte_to_i2c, byte_from_i2c;

I2cReader i2c_reader_i(
    .clk(clk100m_ref),
    .rstp(btn),

    .Done_i(done),
    .AckReceived_i(ack_r),
    .Byte_ib8(byte_from_i2c),
    .SendStartBit_op(start),
    .SendStopBit_op(stop),
    .SendByte_op(sendb),
    .GetByte_op(getb),
    .AckToSend_o(ack_s),
    .Byte_ob8(byte_to_i2c),

    .valid_i(valid),
    .rw_i(rw),
    .ready_o(rd),

    .data_o(byte_from_reader),
    .data_i(byte_to_reader));

I2cMasterGeneric i2c_master_i(
    .Clk_ik(clk100m_ref),
    .Rst_irq(1'b0),
    .SendStartBit_ip(start),
    .SendByte_ip(sendb),
    .GetByte_ip(getb),
    .SendStopBit_ip(stop),
    .Byte_ib8(byte_to_i2c),
    .AckToSend_i(ack_s),
    .Byte_ob8(byte_from_i2c),
    .AckReceived_o(ack_r),
    .Done_o(done),

    .Scl_ioz(i2c_scl),
    .Sda_ioz(i2c_sda));

mcoi_xu5_optics mcoi_xu5_optics_i();

endmodule
