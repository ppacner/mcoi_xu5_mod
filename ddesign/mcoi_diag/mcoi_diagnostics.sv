// TODO license
`timescale 1ns/100ps 

module mcoi_diagnostics(
    input clk,
    input rstn,
    output [15:0] temp_o,
    output [15:0] power_o,
    output [3:0] rev_o,
    output [63:0] id_o,

    input [3:0] revpins_i,
    input rs485_i,
    output rs485_o,
    inout sda_io,
    inout scl_io);

localparam DEVICE_NUM = 4; 
logic [(DEVICE_NUM-1):0] active, fin, fin_cc;
logic [(DEVICE_NUM-1):0] valid, rw;
logic [$clog2(DEVICE_NUM)-1:0] dev;

logic [7:0] byte_from_reader, byte_to_reader;
logic [(DEVICE_NUM-1):0][15:0] byte_to_mux;
logic muxo_valid, muxo_rw, rd;

//multiplexor
always_comb begin
    case(dev)
        2'b00: begin active = 4'b1110;   
                     byte_to_reader = byte_to_mux[0];
                     muxo_valid = valid[0]; 
                     muxo_rw = rw[0]; end
        2'b01: begin active = 4'b1101;
                     byte_to_reader = byte_to_mux[1];
                     muxo_valid = valid[1]; 
                     muxo_rw = rw[1]; end
        2'b10: begin active = 4'b1011;
                     byte_to_reader = byte_to_mux[2];
                     muxo_valid = valid[2];  
                     muxo_rw = rw[2]; end
        2'b11: begin active = 4'b0111;
                     byte_to_reader = byte_to_mux[3];
                     muxo_valid = valid[3];  
                     muxo_rw = rw[3]; end
    endcase end

//device switcher
always_ff@(posedge clk or negedge rstn) begin
    fin_cc <= fin;
    dev <= dev;
    if(!rstn) dev <= '0;  
    else if(fin[dev] && !fin_cc[dev]) //sensitive on posedge
        dev <= (dev == 2'd3) ? 2'd1 : dev + 1; end  

PllConfigLoader pll_config_loader_i (
    .rstp(active[0]),
    .clk(clk),
    .valid_o(valid[0]),
    .rw_o(rw[0]),
    .ready_i(rd),
    .byte_i(byte_from_reader),
    .byte_o(byte_to_mux[0]),
    .fin_o(fin[0]));

I2cDev #(.OUT_SIZE(2)) temp_i(
    .rstp(active[1]),
    .clk(clk),
    .valid_o(valid[1]),
    .rw_o(rw[1]),
    .ready_i(rd),
    .byte_i(byte_from_reader),
    .byte_o(byte_to_mux[1]),
    .data_o(temp_o),
    .fin_o(fin[1]));

I2cDev #(.OUT_SIZE(2)) 
powr_i(
    .rstp(active[2]),
    .clk(clk),
    .valid_o(valid[2]),
    .rw_o(rw[2]),
    .ready_i(rd),
    .byte_i(byte_from_reader),
    .byte_o(byte_to_mux[2]),
    .data_o(power_o),
    .fin_o(fin[2]));

I2cDev #(.OUT_SIZE(8)) 
eeprom_i(
    .rstp(active[3]),
    .clk(clk),
    .valid_o(valid[3]),
    .rw_o(rw[3]),
    .ready_i(rd),
    .byte_i(byte_from_reader),
    .byte_o(byte_to_mux[3]),
    .data_o(id_o),
    .fin_o(fin[3]));

logic done, start, stop, ack_r, ack_s, sendb, getb; 
logic [7:0] byte_to_i2c, byte_from_i2c;

I2cReader i2c_reader_i(
    .clk(clk),
    .rstp(1'b0),

    .Done_i(done),
    .AckReceived_i(ack_r),
    .Byte_ib8(byte_from_i2c),
    .SendStartBit_op(start),
    .SendStopBit_op(stop),
    .SendByte_op(sendb),
    .GetByte_op(getb),
    .AckToSend_o(ack_s),
    .Byte_ob8(byte_to_i2c),

    .valid_i(muxo_valid),
    .rw_i(muxo_rw),
    .ready_o(rd),

    .data_o(byte_from_reader),
    .data_i(byte_to_reader));

I2cMasterGeneric i2c_master_i(
    .Clk_ik(clk),
    .Rst_irq(1'b0),
    .SendStartBit_ip(start),
    .SendByte_ip(sendb),
    .GetByte_ip(getb),
    .SendStopBit_ip(stop),
    .Byte_ib8(byte_to_i2c),
    .AckToSend_i(ack_s),
    .Byte_ob8(byte_from_i2c),
    .AckReceived_o(ack_r),
    .Done_o(done),

    .Scl_ioz(scl_io),
    .Sda_ioz(sda_io));

//TODO connect illa block on uart and check outputs and irqs
// just for fast test
logic irq_r, irq_t;
logic [31:0] i, o, ii, oo;
always@(posedge clk or posedge rstn) begin
    if(!rstn) begin ii <= '0;
                     oo <= '0; end
    else begin ii <= ii;
               oo <= oo;
               if(irq_r) oo <= o + 1; 
               else if(irq_t) ii <= oo; end end

uart#(.clk_freq(40000000)
) uart_i(
    .sys_clk(clk),
    .sys_rst(1'b0),
    .csr_a(14'h000),
    .csr_we(1'b1),
    .csr_di(ii),
    .csr_do(o),
    .rx_irq(irq_r),
    .tx_irq(irq_t),
    .uart_rx(rs485_i),
    .uart_tx(rs485_o));

assign rev_o = revpins_i;

endmodule
