
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, First, FallingEdge, Edge, ReadOnly
from cocotb.drivers import BusDriver
from cocotb.binary import BinaryValue
from cocotb.monitors import BusMonitor

class i2c_bus(BusDriver):
    _signals = ["Sda_ioz", "Scl_ioz"]

    def __init__(self, entity, name, clock):
        BusDriver.__init__(self, entity, name, clock)
        self.clock = clock
        self.bus.Sda_ioz.setimmediatevalue(1)
        self.bus.Scl_ioz.setimmediatevalue(1)

class monitor():
    def __init__(self, dut):
        self.dut = dut

    @cocotb.coroutine
    async def watch_status(self):
        actual = self.dut.i2c_master_inst.State_qb4.value
        print("status: ", actual)

        while True:
            await RisingEdge(self.dut.Clk_ik)
            if (actual != self.dut.i2c_master_inst.State_qb4.value):
                actual = self.dut.i2c_master_inst.State_qb4.value
                print(self.dut.i2c_master_inst.Scl_ioz.value.binstr)

class I2CMonitor():
    '''
    I2C Monitor
    '''
    # _signals = ["Sda_ioz", "Scl_ioz"]

    def __init__(self, entity, name, clock):
        self.bus = entity
        self.clock = clock
        self.sda = self.bus.Sda_ioz
        self.scl = self.bus.Scl_ioz
        self.addr = 0x70
        self.mem_map = [0x00, 0x1F, 0x44, 0x00, 0x81, 0xEC, 0x14,
                        0xEC, 0x15, 0xEC, 0x01, 0x90, 0x54, 0x04,
                        0x00, 0x03]
        self.pointer = 0
        self.rbyte = 0 #receive
        self.sbyte = 0 #send
        self.div = 1025 # clock divider, parameter from design 
        self.prev_scl = 1
        self.ack = 0
        self.rw = 0
        self.first_write = False
        self.inprogress = False
        self.last_state = 0

    @cocotb.coroutine
    def dev_fsm(self):
        state = 0
        while (1):
            yield self.is_start_stop()

            if(self.rw and self.inprogress and state < 1):
                print("SLAVE: read from me")
                self.pointer = self.pointer if self.pointer else 0
                self.sbyte = self.mem_map[self.pointer]
                print("SLAVE: data = ", self.sbyte, "pointer = ", self.pointer)
                yield self.send()
                yield self.get_ack()

                if(self.ack):
                    print("SLAVE: got NACK")
                    self.first_write = False
                    self.inprogress = False
                else:
                    print("SLAVE: got ACK")

                # check if the pointer has right range
                if(len(self.mem_map) > self.pointer ):
                    self.pointer = self.pointer + 1
                else:
                    self.pointer = 0

            else:
                print("reading bus ...")
                state = yield self.recv()

                if(self.is_addr() or self.inprogress and state < 1):
                    yield self.send_ack()
                    if(self.is_addr()):
                        self.inprogress = True
                        self.rw = self.rbyte & 0x01 # 0-write, 1-read
                        print("SLAVE: address valid")

                    elif(not self.rw):
                        print("SLAVE: write into me")
                        if(not self.first_write): 
                            self.pointer = self.rbyte 
                            self.first_write = True
                        else:
                            self.mem_map[self.pointer] = self.rbyte
                            if(len(self.mem_map) > self.pointer ):
                                self.pointer = self.pointer + 1
                            else:
                                self.pointer = 0

    @cocotb.coroutine
    def edge_detector(self, signal):
        if(not self.last_state):
            yield RisingEdge(signal)
        else:
            yield FallingEdge(signal)
        self.last_state = int(signal)

    @cocotb.coroutine
    def is_start_stop(self):
        while(True):
            if(not self.inprogress):
                yield self.edge_detector(self.sda)
                if(not self._sda() and self._scl()):
                    print("SLAVE: Start sequence")
                    return 1
            else:
                break

    def _sda(self):
        return int(self.sda)
    
    def _scl(self):
        return int(self.scl)

    def is_addr(self):
        return self.addr == self.rbyte//2
    
    @cocotb.coroutine
    def wait_for_status(self, status):
        while(not status()):
            yield RisingEdge(self.clock)

    @cocotb.coroutine
    def recv(self):
        self.rbyte = 0
        self.fall = 0
        for i in range(0, 8):
            yield RisingEdge(self.scl)
            self.rbyte = self.rbyte * 2 + self._sda()
            while(True):
                self.fall = self._sda()
                yield RisingEdge(self.clock)
                if(not int(self.scl)):
                    print("SLAVE receiving: ", self.rbyte)
                    break
                elif(not self.fall and self._sda()):
                    self.inprogress = False
                    self.first_write = False
                    print("SLAVE: Stop sequence")
                    return 1
                elif(self.fall and not self._sda()):
                    print("SLAVE: Repeated start sequence")
                    return 2
        return 0

    @cocotb.coroutine
    def send_bit(self, polarity):
        for i in range(0, 2*self.div):
            self.sda <= polarity
            yield RisingEdge(self.clock)

    @cocotb.coroutine
    def send(self):
        yield FallingEdge(self.scl)
        for i in range(0, 8):
            pol = int((self.sbyte & (2**(7-i)))//(2**(7-i)))
            yield self.send_bit(pol)
            print("SLAVE sending: ", self._sda())

    @cocotb.coroutine
    def get_ack(self):
        yield RisingEdge(self.scl) 
        self.ack = self._sda()

    @cocotb.coroutine
    def send_ack(self):
        for i in range(0, 3*self.div):
            yield RisingEdge(self.clock)
            self.sda <= 0 

@cocotb.test()
async def i2c_reader_tb(dut):

    # Start the clock
    clock = Clock(dut.clk, 1, units="ns")
    cocotb.fork(clock.start())  

    i2c_dev = I2CMonitor(dut, name="", clock=dut.clk)
    cocotb.fork(i2c_dev.dev_fsm())

    dut.rstp <= 0 
    dut.valid_i <= 1
    dut.data_bi <= 0xff02 #second register in the table
    dut.rw_i <= 1 #write register
    await FallingEdge(dut.ready_o)
    dut.valid_i <= 0
    
    # wait until block is idle
    while(not int(dut.ready_o)):
        await RisingEdge(dut.clk)

    print(int(dut.data_bo))
