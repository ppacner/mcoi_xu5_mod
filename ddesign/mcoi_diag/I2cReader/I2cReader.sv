//TODO license
`timescale 1ns/100ps 

module I2cReader
#(
    parameter DEV_ADDR=7'h70
)(
    input clk,
    input rstp,

    //i2c master block control
    input Done_i,
    input AckReceived_i,
    input [7:0] Byte_ib8,
    output SendStartBit_op,
    output SendByte_op,
    output GetByte_op,
    output SendStopBit_op,
    output AckToSend_o,
    output [7:0] Byte_ob8,

    input valid_i,
    input rw_i,
    output ready_o,
    output [7:0] data_o,
    input [15:0] data_i //[value[15:8], register[7:0]]
);

localparam TRUE = 1'b1;
localparam FALSE = 1'b0;

//fsm states
localparam IDLE = 3'b000;
localparam STRT = 3'b001; 
localparam ADDR = 3'b010; 
localparam SND0 = 3'b011;
localparam SND1 = 3'b100;
localparam RECV = 3'b101;
localparam SAVE = 3'b110;
localparam STOP = 3'b111;

//shift states
logic [2:0] state, next;
always_ff@(posedge clk or posedge rstp) begin
    if(rstp) state <= IDLE;
    else state <= next; end

//combinational logic
logic done_cc, sloop; //sloop - second loop
always_comb begin
    next = state;
    case(state)
        IDLE: if(valid_i) next = STRT; 
        STRT: if(Done_i && !done_cc) next = ADDR;
        ADDR: if(Done_i && !done_cc) begin
            if(!AckReceived_i && rw_i) next = !sloop ? SND0 : RECV;
            else if(!AckReceived_i && !rw_i) next = SND0; 
            else next = STOP; end
        SND0: if(Done_i && !done_cc) begin
            if(!AckReceived_i && rw_i) next = STRT;
            else if(!AckReceived_i && !rw_i) next = SND1; 
            else next = STOP;
        end
        SND1: if(Done_i && !done_cc) next = STOP;
        RECV: if(Done_i && !done_cc) next = SAVE;
        SAVE: next = STOP;
        STOP: if(Done_i && !done_cc) next = IDLE; //next = !sloop ? STRT : IDLE;
    endcase
end

//control signals
logic start, stop, send_b, recv_b, ack, rd; //recv_b negative if ack received
logic [7:0] data_from_bus, data_to_bus;

assign GetByte_op      = recv_b;
assign SendByte_op     = send_b;
assign Byte_ob8        = data_to_bus;
assign SendStartBit_op = start;
assign SendStopBit_op  = stop;
assign AckToSend_o     = !ack;
assign ready_o         = rd;
assign data_o          = data_from_bus;

always_ff@(posedge clk or posedge rstp) begin
    done_cc <= Done_i;
    sloop <= sloop;
    rd <= FALSE;
    {start, stop, send_b, recv_b, ack} <= {5{FALSE}};

    if(rstp) begin
        rd <= TRUE; 
        sloop <= FALSE;
        data_to_bus <= '0;
        {start, stop, send_b, recv_b, ack} <= {5{FALSE}};
    end else begin
        case(state) 
            IDLE: begin
                {start, stop, send_b, recv_b, ack} <= {5{FALSE}};
                data_to_bus <= '0;
                rd <= TRUE; 
                sloop <= FALSE; end
            STRT: if(Done_i && next == state) start <= TRUE;
            ADDR: begin
                data_to_bus <= {DEV_ADDR, !sloop ? FALSE : TRUE}; //write, read
                if(Done_i && next == state) send_b <= TRUE; end
            SND0: begin
                data_to_bus <= data_i[7:0]; 
                sloop <= TRUE;
                if(Done_i && next == state) send_b <= TRUE; end
            SND1: begin
                data_to_bus <= data_i[15:8]; 
                if(Done_i && next == state) send_b <= TRUE; end
            RECV: if(Done_i && next == state) recv_b <= TRUE;
            SAVE: begin
                sloop <= FALSE;
                data_from_bus <= Byte_ib8; end
            STOP: if(Done_i && next == state) stop <= TRUE;
        endcase
    end
end

endmodule
