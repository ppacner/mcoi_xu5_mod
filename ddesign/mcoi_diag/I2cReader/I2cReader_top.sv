`timescale 1ns/100ps 

module I2cReader_top (
 input clk,
 input rstp,
 input valid_i,
 output ready_o,
 input rw_i,
 input [15:0]data_bi,
 output [7:0]data_bo,

 inout Scl_ioz,
 inout Sda_ioz
);

logic stop, start, done;
logic ack_from_slv, ack_to_slv;
logic recv_b, send_b;
logic [7:0] byte_from_stream, byte_to_stream;

pullup(Scl_ioz);
pullup(Sda_ioz);

I2cReader read_temp_i(
    .clk(clk),
    .rstp(rstp),
    .Done_i(done),
    .AckReceived_i(ack_from_slv),
    .Byte_ib8(byte_from_stream),

    .SendStartBit_op(start),
    .GetByte_op(recv_b),
    .SendByte_op(send_b),
    .SendStopBit_op(stop),
    .AckToSend_o(ack_to_slv),
    .Byte_ob8(byte_to_stream),
    .valid_i(valid_i),
    .ready_o(ready_o),
    .rw_i(rw_i),
    .data_i(data_bi),
    .data_o(data_bo)
);

I2cMasterGeneric i2c_master_inst(
    .Clk_ik(clk),
    .Rst_irq(rstp),
    .SendStartBit_ip(start),
    .SendByte_ip(send_b),
    .GetByte_ip(recv_b),
    .SendStopBit_ip(stop),
    .Byte_ib8(byte_to_stream),
    .AckToSend_i(ack_to_slv),
    .Byte_ob8(byte_from_stream),
    .AckReceived_o(ack_from_slv),
    .Done_o(done),

    .Scl_ioz(Scl_ioz),
    .Sda_ioz(Sda_ioz)
);

endmodule
