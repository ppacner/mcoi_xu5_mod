# I2C READER
This block is the controlling block for I2cMasterGeneric.sv. It performs two
basic functions - read register(reg) and write register(reg, value). With setting
signal rw_i to log0 or log1 you can choose one of these functions. To aprove the 
transaction the signal valid_i has to be set to log1. On the next cycle the
ready_o will drop down. Whenever the transaction is finnished, the ready_o
signal goes high. 

## FOLDER STRUCTURE
```bash
├── cocotb
│   ├── I2cReader_tb.py
│   ├── i2c_reader_waves.gtkw
│   └── Makefile
├── I2cReader.sv
├── I2cReader_top.sv
└── README.md
```
## TEST BENCH
* First unit test is written in python in cocotb. For launching just use the 
commands below this text
* Test bench in VUNIT is not included yet


# TODO
- structuralize the SLAVE class in cocotb

