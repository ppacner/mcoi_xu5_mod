# MCOI XU5 Design
This document serves as description of a new MCOI platform design. It is split
into two parts, first outlines the use case of the platform and the second is
focused on every sheet in the schematic describing all the issues, desired
behavior and uncertainties in the design. At the end of this file you can find
the general expected behavior. 

## Use case
The board has to control 16 motors in real-time. For every motor there are
assigned 2 length stop switches as the feedback. It can be used in different
applications, wherever motor control system is needed. It will be possible to
deploy it in laboratories for smaller experiments In the next revision is
planned loading the internal memory (EMMC) with Linux and make all the
functions accessible via Ethernet interface.

## Schematics sheets
I tried to split the parts of the design into logical blocks which can be easy
to manage in the future, if something has to change. In the last design of
MCOI, there were two boards (GEFE and MCOI front-end) stack on top of each other
and connected via FMC connector.
The GEFE card was used to communicate via optical interface with VFC card and
contained the whole MCOIs digital design, which controlled led array
through LED driver IC and casting data from the optical interface to the
motors.  It was also possible to read back the status of the board like
temperature, states of the motor switches, board ID, board revision, pll locked,
build number, looped data. Due to the limited availability of GEFE cards, it
was necessary to rebuild this platform to by self-reliant. Because of this
matter, we have chosen the enclustra XU5 module as a replacement for GEFE card. 

### xu5_module.sch
The two connectors in this sheet are connecting the MCOI board with ["Mercury XU5
module"](https://download.enclustra.com/public_files/SoC_Modules/Mercury_XU5/Mercury_XU5-R1-2_User_Schematics_V5.pdf).
The majority of the pins are connected to backplane connector (motor_in
and motor_out harness connectors) from where they go to the particular motors. 
Then there is Ethernet connector for further upgrade to be able to set the
motors via Ethernet. For debugging purposes I added UART which is able to
communicate with Linux kernel.
The Board is using a few of I2C ICs and we needed an access to these either
from PL or PS part of the FPGA. Therefore two I2C interfaces for either of the
parts are deployed. The voltage of the bus has to be shifted for the PL part
because of the supply voltage level of the banks which is 1V8. For the PS part
is not necessary because the module already involves one.
The another very important part of the module pins is configuration pins.
According to development kit "Mercury PE1" I realized that there are two
configuration pins (BOOT_MODE0 & BOOT_MODE1) and based on their logic levels I
can choose the boot device for our design. For current revision we will use
QSPI (64MB) ["Mercury XU5 Schematics page
16/22"](https://download.enclustra.com/public_files/SoC_Modules/Mercury_XU5/Mercury_XU5-R1-2_User_Schematics_V5.pdf).
As far as I understand the ["Mercury XU5
Schematics"](https://download.enclustra.com/public_files/SoC_Modules/Mercury_XU5/Mercury_XU5-R1-2_User_Schematics_V5.pdf)
it should not play any role if the boot option is already set while programming
the FPGA via JTAG.  The Module also generates a few signals, which indicate the
actual status of it.  Below you can find used pins in MCOI design including
reset pin signal (The symbol "#" indicate that it is active if grounded).

* PS_DONE - MPSoC device configuration done   
* PS_POR# - MPSoC power-on reset, so with the press button onboard you can
  reboot the board without turning power supply off and on
* PS_SRST# - MPSoC system reset (Onboard used just for JTAG) 
* PWR_GOOD - it goes low if there is a fail on module or the modul is not
  connected
* PWR_EN - this signal serves for disabling outputs of the module dc/dc
  converters, if floating/3V3 - module power enabled

The XU5 module is fed with 12V and after that the module generates following
voltages/currents: 1V8/2A, 2V5/0.5, 3V3/9A. Most of the onboard devices are
connected to 3V3 power supply which means enough current for each of them.
Output "PWR_GOOD" is indicating that the module is prepared. In the user manual is
described how to properly power up the board based on the "PWR_GOOD" and
"PWR_EN" signals ["Mercury XU5 User Manual page
27/26"](https://download.enclustra.com/public_files/SoC_Modules/Mercury_XU5/Mercury_XU5_User_Manual_V07.pdf),
but "PWR_EN" seems not connected in "XU5 Schematics" although the "Mercury_XU5_User_Manual" on page
32 says that it can turn off the generated power supplies (1V8, 2V5, 3V3
etc...) and the development kit PE1 manual says that if the "System monitor" is not
utilized reset circuit needs to be used. But on MCOI board I let it unconnected
because in consonance with "Mercury_XU5_User_Manual_V07" page 33 the "PWR_EN"
can be left unconnected, so current design is using "PWR_GOOD" for indication
of proper working state of the module while powering up of the board and FPGA
is controlled by voltage supervisor IC. I wanted to make my board completely
independent on the changes in enclustra modul design (e.g. if the "PWR_GOOD"
will not be available anymore) so I do not have to change mine.  
By using the voltage supervisor IC the board is on whenever power levels
reach certain voltages. Only thing I was worried about was the delay after
power-up between "PWR_GOOD" high and connecting the fpga to power supply which
is set to 10 ns on the reset circuit in ["Mercury PE1
Schematics page 17/23"](https://download.enclustra.com/public_files/Base_Boards/Mercury+_PE1/Mercury_PE1-R4-6_User_Schematics_V1.pdf)
but as far as I am not using the "PWR_EN" it should not be mandatory. Even then
the implemented voltage supervisor on MCOI has delay even longer about 200ms.

### board_diagnostics.sch
Board diagnostics contains all devices permanently monitoring all MCOI status.
The temperature sensor has unconnected "Alert" pin, because on the PL part is
no unoccupied pin so the only option is to connect it to PS part, which would
not be probably useful. 
The EEPROM memory is used because of the 64-bit internal number, which serves
as a unique identifier of the platform.
Status and test LEDS are inherited from last design. I just changed the values
of resistors for some of them, because of the maximum current for FPGA pins. 

### ethernet_interface.sch
Ethernet schematics is assembled based on the development kit ["Mercury PE1
Schematics page
8/23"](https://download.enclustra.com/public_files/Base_Boards/Mercury+_PE1/Mercury_PE1-R4-6_User_Schematics_V1.pdf)
and official ["KSZ9031RNX
docs"](http://ww1.microchip.com/downloads/en/devicedoc/00002117f.pdf) and
inspired by the development kit layout ["KSZ9031RNX eval-kit page
5/8"](http://ww1.microchip.com/downloads/en/DeviceDoc/KSZ9031RNX%20Eval-Sckt%20Board%20Rev1_1,%20Sch%20Rev1_2.pdf).
I was concerned about the pin mapping on the Ethernet connector, because in the
evaluation board schematics the do not connect pin straight, but swap the 4th
and the 5th pin but I also found pictures of the straight connection in the
internet.    

### optical_interface.sch
Optical interface replaces GBTx chip from GEFE card. It includes PLL for
feeding the FPGA transceiver clock pins. There was a problem while running
tests of "gbt_fpga" design on the development kit "Mercury PE1". There was a
crystal oscillator with precision of 50ppm which apparently was not enough,
after connecting the output of the si5338-evb it worked. I could not find the
exact oscillator from the EVB so I have chosen precision about 10ppm to be
sure, because on the GEFE card there was precision about 20ppm.
Then there is the SFP connector. I connected the additional outputs to I2C bus
with the idea to check the working state of optical link e.g. while testing the
board features and add the coupling capacitors on the high speed lines as it
was in the design of SFP mezzanine
[CERN mezzanine](https://edms.cern.ch/ui/#!master/navigator/item?I:1428891054:1428891054:subDocs).
Voltage levels are exactly choose as in "Mercury PE1", where they feed the
MGT with 3V3 logic signals even though the voltage of the MGT bank is supplied
with 0.9V. I didn't find any lead how is this possible, but it worked on the
evaluation board. 

### power_block.sch
The board is supplied from 12V. It goes through PPTC (500mA/30V/15W), I don't
expect more than 15W consumption, because while I was running the tests on
development kit I got 9W. I am still not sure, how much will be the consumption
with the PL and PS part on together.
It follows over-voltage protection which controls the right voltage on the input of
the board and protects the board up to -60 V reverse voltage.  Probably the
diodes on the input would be sufficient but it can't control the over-voltage
which will be useful in laboratories if someone accidentally set the wrong
voltage which sometimes happens. After that if it comes through the over-voltage
protection it goes next to the shunt from power monitor and than to Module and
to the transistors which are activating the onboard power supply
(https://www.diodes.com/assets/Datasheets/ds31946.pdf). These transistors could
seem a little bit small but according to table in ["Mercury XU5 User Manual page
32/64"](https://download.enclustra.com/public_files/SoC_Modules/Mercury_XU5/Mercury_XU5_User_Manual_V07.pdf)
in the power requirements do not have to be high. For the full board
functionality following power supplies are needed +1V8, +3V3, +5V and 12V. I
removed the +2V5 from further usage but it is still connected to the voltage
supervisor because it reflects the correct working state of the module.

### serial_interface.sch
For further upgrade of this platform we need access to serial console from PC. For
this reason I placed the FTDI chip USB to serial UART. There has to be a bus
level shifter (the same IC as for I2C deployed), because the bank is connected
to 1V8 and bus volatge is 3V3. If any configuration via dedicated FTDI utility
is needed, the additional EEPROM is onboard.  The sheet includes also RS485
transceiver, I added 120 ohms resistor to define differential impedance
according to documentation from TI ["RS485 Design
Guide"](http://www.ti.com/lit/an/slla272c/slla272c.pdf?ts=1590322521419). I

### backplane_connector.sch
In this block you can see the direct connections from module to the connector.

#### backplane_buffer.sch
This part is nothing but translation of the voltage levels from outside of
board to "VADJ" voltage which is voltage used for PL banks of FPGA. Actually
there is a mistake in the chosen voltage for "VADJ" in actual version of
schematics, because "VADJ" is 2V5 instead of 1V8.     

#### switch_filter.sch
Switch filters are deployed for removing the bouncing from the push-buttons,
which are used as length stops. This part is also reused from last revision of
MCOI with GEFE.  

### led_register.sch
This part is just a wrapper for re-utilized "MCOI LED bar" from last revision of
the MCOI with GEFE card. I created this wrapper to be able to harness all the
pins into one connector without modifying the file from different git submodule.
This block includes LED array indicating status of all the 16 motors. It's made
of dual color LED for displaying particular state and it varies either
if the LED is blinking or not. From outside of the board through backplane
connector come signals with 5V logic so there is also dual-supply bus
transceiver implemented for that. All the tespoints are from the last design.     

## Expected behavior
Expected behavior of the board boot-up: after connecting the main
power supply to the backplane or to the terminal block with the right
level and polarity it will go next to the module which will generate 3 voltages
(3V3/2V5/1V8). Than if they reach the stable state, the IC power supervisor
opens transistors and the power gets to the module FPGA and all the other
components on board. The board should be than accessible through JTAG after
that, so it can be loaded with a digital design. If the module has onboard
failure or is not connected the Fail LED will glow and LED PS_DONE, MOD,
+5V will be off. If you press PS_POR it resets all the power supplies on
the module and the onboard voltages either.  

## Sources
* ["Mercury XU5 User Manual"](https://download.enclustra.com/public_files/SoC_Modules/Mercury_XU5/Mercury_XU5-R1-2_User_Schematics_V5.pdf)
* ["Mercury XU5 Schematics"](https://download.enclustra.com/public_files/SoC_Modules/Mercury_XU5/Mercury_XU5-R1-2_User_Schematics_V5.pdf)
* ["Mercury PE1 Schematics"](https://download.enclustra.com/public_files/Base_Boards/Mercury+_PE1/Mercury_PE1-R4-6_User_Schematics_V1.pdf)
* ["KSZ9031RNX Schematics"](http://ww1.microchip.com/downloads/en/DeviceDoc/KSZ9031RNX%20Eval-Sckt%20Board%20Rev1_1,%20Sch%20Rev1_2.pdf)
* ["KSZ9031RNX datasheet"](http://ww1.microchip.com/downloads/en/devicedoc/00002117f.pdf)
* ["CERN mezzanine"](https://edms.cern.ch/ui/#!master/navigator/item?I:1428891054:1428891054:subDocs)
* ["RS485 Design Guide"](http://www.ti.com/lit/an/slla272c/slla272c.pdf?ts=1590322521419)
* ["KSZ9031RNX eval-kit"](http://ww1.microchip.com/downloads/en/DeviceDoc/KSZ9031RNX%20Eval-Sckt%20Board%20Rev1_1,%20Sch%20Rev1_2.pdf)
* ["KSZ9031RNX docs"](http://ww1.microchip.com/downloads/en/devicedoc/00002117f.pdf)

## Discussion
* Are you really sure to use L1 and L2? What are the advantages in respect to
  the reliability reduction by adding them?


* You have introduced the ESD protection for the JTAG lines, but what about ESD
  protection for the I2C lines which are routed to the connector?  
  I added I2C protection on "board diagnostics" sheet and also RS485 end
  protection.

* The use of AC decoupling capacitors for SFP modules is highly not recommended and not needed.
I adjusted the output voltage from external pll "si5338" to 3V3 as they did on
the development board from enclustra [PE1 development
kit](https://download.enclustra.com/public_files/Base_Boards/Mercury+_PE1/Mercury_PE1-R4-6_User_Schematics_V1.pdf).
The voltage of the bank with MGT is 0V9.

* Not sure about the following MOSFETs connection, could you please re-check it?
According to typical application in datasheet it is correct
[LTC4365ITS8](https://www.analog.com/media/en/technical-documentation/data-sheets/LTC4365.pdf).

* No filtering capacitor is required at the output of IC6?
I used the design from [development kit
PE1](https://download.enclustra.com/public_files/Base_Boards/Mercury+_PE1/Mercury_PE1-R4-6_User_Schematics_V1.pdf),
because it seems the capacitors are deployed because of the voltage of MGT bank
which is 0V9. The capacitors are decreasing voltage level of the signal.
* Why so many tests points on static (pulled down) and unused (I think) pins in IC4? 
It was used for debuging of the previous board and I just reused this sheet
from last project as submodule.

* Not recommended to have LC filters on the power supply like you did with
L6, L7 and C52. It can play as a resonator, so a PI filter is more appropriated
(so it depends on how you place the components on the layout). 
// TODO

