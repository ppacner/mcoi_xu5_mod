# HW PLATFORM DEVELOPMENT
Current hardware platform is controlled with module mercury xu5 from enclustra.
To warrant the compatibility in case of pin names reused from general design the notation from enclustra
[module datasheet](https://download.enclustra.com/public_files/SoC_Modules/Mercury_XU5/Mercury_XU5-R1-2_User_Schematics_V5.pdf) is used.
The changes in current design refering to the last design as new input/output
pins, new connectors or features are written in section CHANGELOG.

### REQUIREMENTS
- add 120 MHz oscillator
- SFP pairs matched
- for storing linux -> use onboard memory (test load firmware before layout fabrication)
- remove usb3 from ddesign
- ldo 3v3, 2V5, 1V8 
- __sfp connector - three feet remove not to interfere with light guide__
- heat sink -> common (old graphics) attached via plastic pins
- step-downs won't be probably needed
- for programming -> design small board (10pin smt box <==> samtec sfmh conn)
- connect sfp enable to fpga (start sending/receiving after clock launch)
- add testpoints and si5338 bypass
- overvoltage zener diode

### Bill of materials
| Component name  | Type | Purchase | CERN Altium libs | Notes |
| :---: | :---:| :---: | :---: | :---: |
| [LMZ12003TZ-ADJ/NOPB](http://www.ti.com/lit/ds/symlink/lmz12003.pdf) | DC/DC converter | [link](https://ch.farnell.com/texas-instruments/lmz12003tz-adj-nopb/ic-power-module-3a-to-pmod-7/dp/3008303?st=LMZ12003)  | :white_square_button: | 12V -> 5V/2A/ dis. max 2W |
| [6339191-1](http://www.farnell.com/datasheets/1931995.pdf) | ethernet connector | [link](https://uk.farnell.com/amp-te-connectivity/6339191-1/modular-jack-rj45-cat5-smt/dp/2131151) | :white_square_button: | - |
| [7490100111A](http://www.farnell.com/datasheets/2616615.pdf) | ethernet transformator | [link](https://ch.farnell.com/wurth-elektronik/7490100111a/transformator-1-port-350uh-8ma/dp/2401059)  | :white_square_button: | - |
| [FX10A-168P-SV(91)](http://www.farnell.com/datasheets/2691101.pdf) | pitch stacking connectors | [link](https://ch.farnell.com/hirose-hrs/fx10a-168p-sv-91/stiftleiste-stapelbar-168pos-0/dp/2498872)  | :white_square_button: | stacking high 4mm => spacer 4.3 +- 0.127mm |
| [10118193-0001LF](https://www.farnell.com/cad/1935764.pdf) | usb connector | [link](https://ch.farnell.com/amphenol-icc-fci/10118193-0001lf/micro-usb-buchse-2-0-typ-b-smt/dp/2751675)  | :white_square_button: | - |
| [FT232HL-REEL](https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT232BL_BQ.pdf) | interface bridge | [link](https://uk.farnell.com/ftdi/ft232bl-reel/ic-usb-to-uart-smd-32lqfp/dp/9519769) | :white_square_button: | USB -> UART |
| [FSH-105-04-L-DH](https://www.mouser.ch/datasheet/2/527/fsh-1369930.pdf) | 10pin SMT connector (male) | [link](https://www.mouser.ch/ProductDetail/Samtec/FSH-105-04-L-DH?qs=sGAEpiMZZMs%252BGHln7q6pm8Vn94ktop%2FJFxTHMAEdKYq8Ww8guyFeNQ%3D%3D)  | :white_square_button: | JTAG |
| [SFMH-105-02-L-D-LC](https://www.mouser.ch/datasheet/2/527/sfmh_sm-1370154.pdf) | 10pin SMT connector (female) | [link](https://www.mouser.ch/ProductDetail/Samtec/SFMH-105-02-L-D-LC?qs=sGAEpiMZZMs%252BGHln7q6pm8Vn94ktop%2FJql8rifDPWDqaFqRZEn%2FODw%3D%3D)  | :black_square_button: | - |
| [N2510-5002RB](http://www.farnell.com/datasheets/746400.pdf) | 10pin SMT box connector | [link](https://uk.farnell.com/3m/n2510-5002rb/header-right-angle-10way/dp/9838350?st=3M%20n2510)  | :white_square_button: | - |
| [HSMG-A100-L02J1](http://www.farnell.com/datasheets/2095942.pdf) | LED | [link](https://ch.farnell.com/broadcom-limited/hsmg-a100-l02j1/led-smd-plcc2-gr-n/dp/1830409?st=HSMG-A100-L02J1)  | :white_square_button: | green (smd) |
| [597-3401-207F](http://www.farnell.com/datasheets/1783318.pdf) | LED | [link](https://ch.farnell.com/dialight/597-3401-207f/led-round-7mcd-yellow-smd-full/dp/2410202)  | :white_square_button: | - |
| [CLM1B-BKW-CTBVA353]()| LED | [link](https://ch.farnell.com/cree/clm1b-bkw-ctbva353/smd-lm1-b-series-led-blue-color/dp/3218598) | :black_square_button: | - |
| [1279.1001]()| light guide | [link](https://ch.farnell.com/mentor/1279-1001/lichtleiter-4fach-horizontal-3mm/dp/2300578)  | :black_square_button: | - |
| [1279.1002]()| light guide | [link](https://ch.farnell.com/mentor/1279-1002/lichtleiter-4fach-horizontal-3mm/dp/2300579)  | :black_square_button: | - |
| [DVIULC6-4SC6](http://www.farnell.com/datasheets/1689766.pdf)| ESD IC | [link](https://ch.farnell.com/stmicroelectronics/dviulc6-4sc6/esd-schutzdiode-array-bidir-sot23/dp/1751961)  | :white_square_button: | - |
| [SI5338A-B-GM]()| PLL | [link](https://uk.farnell.com/silicon-labs/si5338a-b-gm/clock-generator-710mhz-qfn-24/dp/2421794?ost=si5338a&ddkey=https%3Aen-GB%2FElement14_United_Kingdom%2Fsearch)  | :white_square_button: | - |
| [CLVCH16T245MDGGREP]()| translator | [link](https://www.mouser.ch/ProductDetail/Texas-Instruments/CLVCH16T245MDGGREP?qs=%2Fha2pyFaduhzkLo219%2FRY4SacfKMrnerGXdB5yHFuLdxO2cH%2F1Ag%2FPx0JeTY%252Bayu&utm_source=octopart&utm_medium=aggregator&utm_campaign=595-VCH16T245MDGGREP&utm_content=Texas%20Instruments)  | :white_square_button: | behind DIN connector |
| [TSX-3225 25.0000MF10P-C3]()| clock IC | [link](https://www.mouser.ch/ProductDetail/Epson-Timing/TSX-3225-250000MF10P-C3?qs=sGAEpiMZZMsBj6bBr9Q9abOFdSDy7migipt5D%2FtLRvmRj1rYWPr39w%3D%3D)  | :white_square_button: | - |
| [2227303-3]()| SFP | [link](https://www.mouser.ch/ProductDetail/TE-Connectivity-AMP/2227303-3?qs=%2Fha2pyFaduilaw2T32rfXTEVcYrdAj%2FEW%252B8xmvdbdvMOgnAof0wWmw==&utm_source=octopart&utm_medium=aggregator&utm_campaign=571-2227303-3&utm_content=TE+Connectivity)  | :black_square_button: | 1.8mm pin length, still not in libraries |
| [MAX13430EETB+T]()| Transceiver IC  | [link](https://ch.farnell.com/maxim-integrated-products/max13430eetb-t/rs485-transceiver-500kbps-5-5v/dp/2511925?st=MAX13430E)  | :white_square_button: | RS485 |
| [BLM18KG121TZ1D]()| ferrite bead | [link](https://www.mouser.ch/ProductDetail/Murata-Electronics/BLM18KG121TZ1D?qs=sGAEpiMZZMtdyQheitOmRQALsn%2F4R2uWtfpogYgCFDRMABFlTTRuxg%3D%3D)  | :white_square_button: | - |
| [MCP9808-E/MS]()| thermal sensor | [link](https://ch.farnell.com/microchip/mcp9808-e-ms/temp-sensor-i2c-20-b-100-c-8msop/dp/2080523?st=MCP9808)  | :white_square_button: | I2C bus |
| [PCA9306DQER]()| level shifter | [link](https://ch.farnell.com/texas-instruments/pca9306dqer/volt-level-translator-i2c-smbus/dp/3009537?st=PCA9306) | :white_square_button: | - |
| [SMBJ12CA]()| transient voltage suppressor | [link](https://www.mouser.ch/ProductDetail/Taiwan-Semiconductor/SMBJ12CA?qs=sGAEpiMZZMvS4F1mNSR4OnrjkxvIFSBn)  | :white_square_button: | 12A zener diode |
| [DLW5BTM501SQ2L]()| transformer | [link](https://www.mouser.ch/ProductDetail/Murata-Electronics/DLW5BTM501SQ2L?qs=HwwSmNcRtL56ay6tSJuAZA==)  | :white_square_button: | - |
| [MKDS 1/ 2-3,5 HT BK]()| terminal block | [link](https://fr.farnell.com/phoenix-contact/mkds-1-2-3-5-ht-bk/bornier-fil-a-carte-2-voies-16awg/dp/2766173?st=1985807) | :white_square_button: | - |
| [INA219BIDCNT](https://www.ti.com/lit/ds/symlink/ina219.pdf)| current sensor | [link](https://ch.farnell.com/texas-instruments/ina219bidcnt/ic-current-shunt-monitor-8sot23/dp/3005352?st=INA219) | :white_square_button: | - |
| [LTC4365ITS8#TRMPBF]()| overvoltage protection | [link](https://www.mouser.ch/ProductDetail/Analog-Devices/LTC4365ITS8TRMPBF?qs=sGAEpiMZZMtFLzWzeHO23HwmFp%2FnrpyK4ESfXQEz8uk%3D) | :white_square_button: | - |
| [SISB46DN-T1-GE3]()| power transistor | [link](https://www.mouser.ch/ProductDetail/Vishay-Siliconix/SISB46DN-T1-GE3?qs=%2Fha2pyFaduis41OrFNtjahCykIDZpUfMY2ffUblUTDY%3D) | :white_square_button: | - |
| [SN74ALVCH16646DL](http://www.ti.com/general/docs/suppproductinfo.tsp?distId=26&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74alvch16646)| registered transceiver | [link](https://www.mouser.ch/ProductDetail/Texas-Instruments/SN74ALVCH16646DL?qs=%2Fha2pyFadugoF%252B4lmBlcD2MVPlVvIArrWkX7FcpvRJ6paYq9GBLGcimgWaTQCHGh&utm_source=octopart&utm_medium=aggregator&utm_campaign=595-SN74ALVCH16646DL&utm_content=Texas%20Instruments) | :white_square_button: | maybe for future update |
| [SN74CBT16233DLR](http://www.ti.com/lit/ds/symlink/sn74cbt16233.pdf)| multiplexor, switch | [link](https://www.mouser.ch/ProductDetail/Texas-Instruments/SN74CBT16233DLR?qs=3pnr37ZAbK%252B65dOnuzCuyQ==) | :white_square_button: | maybe for future update |
| [MBRA340T3G](https://www.onsemi.com/pub/Collateral/MBRA340T3-D.PDF)| schottky diode | [link](https://ch.farnell.com/on-semiconductor/mbra340t3g/schottky-diode-3a-40v-sma/dp/1431078?st=MBRA340T3G)| :white_square_button: | - |
| [MF-USML200/12-2](http://www.farnell.com/datasheets/2792890.pdf)| PTC fuse | [link](https://ch.farnell.com/bourns/usml200-12-2/ptc-sicherung-r-ckstellb-2a-12v/dp/3052070?st=usml200) | :white_square_button: | - |
| [93LC56BT-I/OT](http://ww1.microchip.com/downloads/en/devicedoc/21794f.pdf) | EEPROM | [link](https://ch.farnell.com/microchip/93lc56bt-i-ot/serial-eeprom-2kbit-2mhz-sot-23/dp/1556165?st=93LC56B) | :white_square_button: | 2Kbit, 2Mhz |
| [2N7002CK](https://assets.nexperia.com/documents/data-sheet/2N7002CK.pdf)| N Power MOSFET | [link](https://uk.farnell.com/nexperia/2n7002ck-215/mosfet-n-ch-60v-0-3a-sot23/dp/1829183?st=2N7002CK) | :white_square_button: | - |
| [DMP2066LSN-7](https://www.mouser.ch/datasheet/2/115/ds31467-64559.pdf)| P MOSFET | [link](https://www.mouser.ch/ProductDetail/Diodes-Incorporated/DMP2066LSN-7?qs=%2Fha2pyFaduhfiNORwV%2F1xWPiX7nazaR%252B8kMFYBHyNmLodjjIlApVIw%3D%3D) | :white_square_button: | - |
| [STPS2L60A](http://www.farnell.com/datasheets/2307436.pdf)| Schottky rectifier | [link](https://ch.farnell.com/stmicroelectronics/stps2l60a/schottky-gleichrichter-2a/dp/1702810?st=stps2l60a) | :white_square_button: | replace by MBRA340T3G??? |
| [ESD5Z2.5T1G](http://www.farnell.com/datasheets/1878252.pdf)| Voltage suppressor | [link](https://ch.farnell.com/on-semiconductor/esd5z2-5t1g/tvs-diode-120w-2-5v-unidir-sod/dp/2533137?st=ESD5Z2.5T1G) | :white_square_button: | - |
| [74LVC1G14GW](http://www.farnell.com/datasheets/2185284.pdf)| Schmitt-Trigger | [link](https://ch.farnell.com/nexperia/74lvc1g14gw-125/logik-74lvc1g-inverter-umt5/dp/1631687RL?st=74LVC1G14GW,125) | :white_square_button: | - |
| [BSS123](http://www.farnell.com/datasheets/2288251.pdf)| N MOSFET | [link](https://ch.farnell.com/on-semiconductor/bss123/mosfet-n-kanal-100v-sot-23/dp/9845321?st=bss123) | :white_square_button: | - |
| [AT24MAC602-XHM-B](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8807-SEEPROM-AT24MAC402-602-Datasheet.pdf) | 2Kbit-Serial EEPROM | [link](https://eu.mouser.com/ProductDetail/Microchip-Technology-Atmel/AT24MAC602-XHM-B?qs=sGAEpiMZZMuVhdAcoizlResxdM4lyyc5O2WA7ZQdx6E%3D)| :white_square_button: |64-bit ID, not bought yet |
| [TPS3307-25](https://www.ti.com/lit/ds/symlink/tps3307.pdf) | Power Supervisor | [link](https://fr.farnell.com/texas-instruments/tps3307-18dgn/voltage-supervisor-40ua-6v-hvssop/dp/3123897?st=TPS3307) | :white_square_button: | not bought yet |
| [PCB-55(M3)](https://www.tme.eu/Document/0c814e212532c91d0fc3fbdcf27c7505/PCB-55%28M3%29.pdf)| mounting bracket | [link](https://www.tme.eu/en/details/pcb-55m3/solder-terminals-pcb-mount/fix-fasten/pcb-55-m3/)  | :black_square_button: | not bought yet |
| [ETTINGER 05.60.223](http://www.farnell.com/datasheets/22766.pdf)| mounting bracket | [link](https://uk.farnell.com/ettinger/05-60-223/cube-standoff-threaded-m2-5-m3/dp/1466865)  | :black_square_button: | not bought yet |
| []()| heat sink | [link]() | :black_square_button: | - |

## Power on board "kit" (A, B connector)
### Power pins from PE1 to XU5 module
| NAME | pins | voltage | direction | notes |
| :---: | :---:| :---: | :---: | :---: |
| VCC_MAIN_MOD | A1-A11, A2-A8 | 5-15V, nom. 12V | out | - |
| VCC_IO_A | A38, A41, A74, A77 | 1V8 | out | - |
| VCC_IO_B | B64, B67, B88, B95, B140, B143 | 1V8 | out | - |
| VCC_USBMOD_P | A131 | - | out | removed |
| VCC_3V3_MOD | A26, A29, A50, B55, B79, A86, B115, B127, B152, B155 | - | in | - |
| VCC_OUT_A | A53, A62, A65, A89 | - | in | - |
| VCC_OUT_B | B52, B74, B108, B128 | - | in | - |
| VCC_BAT | A168 | - | in | connect to GND |

### Power pins from XU5 module to PE1
| NAME | pins | voltage | direction | notes |
| :---: | :---:| :---: | :---: | :---: |
| VCC_IO_BO | A41 | - | in | - |
| VCC_IO_BN | A38 | - | in | - |
| VCC_CFG_MIO | A74, A77 | - | in | - |
| VCC_IO_B66 | B67, B95, B143 | - | in | - |
| VCC_IO_B65 | B64, B88, B140 | - | in | - |
| VCC_BAT | A168 | - | in | not connected |
| VCC_MOD | A1,A3, A5, A7, A9, A11, A2, A4, A6, A8 | - | in | - |
| VCC_1V8 | B52, B76, B108, B128 | 1V8 | out | - |
| VCC_2V5 | A53, A62, A65, A89 | 2V5 | out | - |
| VCC_3V3 | A26, A29, A50, B55, B79, B86, B115, B127, B152, B155 | 3V3 | out | - |

### Other power pins
| NAME | pins | voltage | direction | notes |
| :---: | :---:| :---: | :---: | :---: |
| VCC_CS_B | - | 1V8 | out | - |
| VCC_3V3 | - | 3V3 | out | - |
| VCC_CS_A | - | 2V5 | out | - |

### List of power supplies
* 5V
	- i2c_led_driver (TLC5920DLG4)
* 3V3 (from module)
	- SFP
* 2V5 (from module)
	- switch_filter 

## PLATFORM SETUP
This part is setting summary of important ICs on board. It's intended for
debugging purposes. 
### Step-down converter (12V/5V/3A)
* min. enable voltage 5.3V
* freq. about 430kHz

### Overvoltage protection IC
* Vos = 0.3V
* UVth = 6V
* Iuv = 10uA
* OVth = 12V

# OTHER NOTES
* for module voltages needed ... 12V
* VCC__"Xvoltage_" -> connected throught kit to the module again
* JTAG pull-ups are already included on module platform
* for mcoi 5V logic (2x step-down dc/dc)

# CHANGELOG 
* removed pins - TEST6, TEST7 (from TEST CONNECTOR - board_diagnostics)

## Altium project settings
* ignoring "\<NetName\> contains \<ObjectType1\> and \<ObjectType2\> objects (\<Reference\>)" 
* ignoring "Nets wth no driving source"
* ignoring "Nets with multiple names"
* ignoring "Adding Items from hidden net to net"
* ingoring "Unused sub-part in component"

